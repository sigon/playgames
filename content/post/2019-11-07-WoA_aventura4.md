---
title: Wrath Of Ashardalon, aventura 4
subtitle: Dungeons & Dragons Adventure System
date: 2019-11-07
tags: ["Dungeons & Dragons", "Warth of Ashardalon", "Aventuras"]
#bigimg: [{src: "/reglamentos/img/post/guerrero.png", desc: "guerrero"}]
---

![WoA](/img/WoA/WoA_icon.jpg)

### Aventura 4
#### LA CAMARA MISTERIOSA:

![WoA](/img/WoA/aventura4_camara_misteriosa.png)

Al Duo Dinámico se le ha unido en esta aventura la **elfa paladina Keyleh**. Y gracias a ella, que se ha enfrentado valientemente durante varias ocasiones al **Draco de rabia enfurecido**, han conseguido vencer al Draco.


![Plano detalle de los vencedores](/img/WoA/aventura4_2019-11-07_heroes.jpg "Heroes victoriosos")


Ha sido por los pelos, solo quedaba un 'healing surge' y estaban todos con 1HP, salvo Keyleh, que tenía dos porque hacía poco que habia usado un healing surge.

Muy divertido el enfrentamiento con el Drake.


<!--more-->
![Dungeon al final de la Aventura 4](/img/WoA/aventura4_2019-11-07.jpg "title")


![Cartas de habilidades usadas](/img/WoA/aventura4_2019-11-07_heroes_cartas.jpg "title")
