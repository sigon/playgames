---
title: Pintar aliens
subtitle: pintar miniaturas
date: 2021-10-02
tags: ["paint"]
draft: false
---

![Aliens](/img/post/aliens1.jpg)

Aliens:

* Imprimación en negro - Black Primer (Vallejo)
* Pincel seco con Field Blue (Vallejo)
* Pincel seco con Intermediate Blue (Vallejo)
* capa de Nuln Oil (Games Workshop)

![Aliens](/img/post/aliens2.jpg)

![Reina Alien](/img/post/aliens-reina.jpg)

Bases:

* Balthasar Gold + marrón
* Capa de Nuln oil

Barniz mate (Vallejo)
