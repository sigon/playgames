---
title: ANOTHER GLORIOUS DAY IN THE CORPS
subtitle: game soundtrack
date: 2021-02-08
tags: ["sound", "aliens"]
draft: false
---

![Aliens: Another Glorious Day in the Corps! ](/img/post/ALIENS-Cover-Core.jpg)

Aliens. Full Soundtrack:

{{< audio soundtrack="agditc-soundtrack" >}}


Alien.Isolation Full Soundtrack:

{{< audio soundtrack="isolation-soundtrack" >}}
