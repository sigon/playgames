---
title: Wrath Of Ashardalon
subtitle: Dungeons & Dragons Adventure System
date: 2019-10-30
tags: ["Dungeons & Dragons", "Warth of Ashardalon"]
#bigimg: [{src: "/reglamentos/img/post/guerrero.png", desc: "guerrero"}]
---



Ficha del juego en [Board Game Geek:](https://boardgamegeek.com/boardgame/66356/dungeons-dragons-wrath-ashardalon-board-game)

Reglamento y cartas traducidos al castellano de la [BSK](https://labsk.net/index.php?topic=69095.0)

[![Descargar el Libro de reglas](/img/WoA/WoA_icon.jpg)](https://gitlab.com/sigon/playgames/raw/master/static/pdf/woa/Reglamento%20La%20Ira%20de%20Ashardalon.pdf)


[![Descargar Libro de aventuras](/img/WoA/libro_aventuras.png)](https://gitlab.com/sigon/playgames/raw/master/static/pdf/woa/aventuras%20ashardalon.pdf)


* [cartas heroes 01-09](https://gitlab.com/sigon/playgames/raw/master/static/pdf/woa/cartas/Cartas%20ashardalon%2001-09.jpg)
* [cartas 10-18](https://gitlab.com/sigon/playgames/raw/master/static/pdf/woa/cartas/Cartas%20ashardalon%2010-18.jpg)
